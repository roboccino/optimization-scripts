from bayes_opt import BayesianOptimization
from bayes_opt import UtilityFunction

# x - mixing time
# y - mixing speed
# w - ramp height
pbounds = {'x': (10, 60), 'y': (50, 100), 'w': (0, 90)}

optimizer = BayesianOptimization(
        f = None,
        pbounds = pbounds,
        verbose = 2,
        random_state = 1)

utility = UtilityFunction(kind='ucb', kappa=8, xi=0.1)

# uncomment and fill if the script stopped working and needs to be rerun
'''
results = []
for el in results:
    next_point = optimizer.suggest(utility)
    print(f'Next point to test: mixing time = {round(next_point["x"])}, mixing speed = {round(next_point["y"])}, ramp height = {round(next_point["w"])}')
    print(el)
    optimizer.register(params=next_point, target=el)
'''

for _ in range(60):
    next_point = optimizer.suggest(utility)
    print(f'Next point to test: mixing time = {round(next_point["x"])}, mixing speed = {round(next_point["y"])}, ramp height = {round(next_point["w"])}')
    try:
        target = float(input('Type the result: '))
        optimizer.register(params=next_point, target=target)
    except:
        target = float(input('Try again: '))
        optimizer.register(params=next_point, target=target)
        
print(optimizer.max)

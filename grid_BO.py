from bayes_opt import BayesianOptimization
from bayes_opt import UtilityFunction
# x - mixing time
# y - mixing speed


pbounds = {'x': (0, 90), 'y': (50, 100)}

optimizer = BayesianOptimization(
        f = None,
        pbounds = pbounds,
        verbose = 2,
        random_state = 1,
        )

utility = UtilityFunction(kind='ucb', kappa=8, xi=0.1)


for _ in range(60):
    next_point = optimizer.suggest(utility)
    print(f'Next point to test is: {next_point}')
    try:
        target = float(input())
    except:
        print('Type again')
        target = float(input())
    optimizer.register(params=next_point, target=target)
        
print(optimizer.max)

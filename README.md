# Optimization scripts

This repository contains optimization scripts used in [Roboccino](https://gitlab.com/roboccino/roboccino) project. The objective was to find optimal parameters resulting in the best cappuccino. Cappuccino's quality was evaluated with a vision system - the percentage of bubble coverage in the foam was the quality indicator. The considered parameters (water pouring height, stirring speed and time) were suggested by the scripts after each iteration.

Three types of optimizers were used: Bayesian, Tree Parzen Estimator and custom grid Bayesian. You can read more about the optimization process in my [thesis](https://emilia-szymanska.gitlab.io/cv/docs/7_sem/BSc_thesis.pdf). 
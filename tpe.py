from hyperopt import tpe, hp, fmin
import numpy as np
import os
import pickle as p

def objective(params):
    time, speed, h = params['time'], params['speed'], params['h']
    print('-------------------------------------')
    print(f'time:   {int(round(time))}')
    print(f'speed:  {int(round(speed))}')
    print(f'height: {int(round(h))}')
    try:
        value = float(input())
    except:
        print('Try again')
        value = float(input())

    return value


space = {
    'time': hp.uniform('time', 10, 60),
    'speed': hp.uniform('speed', 50, 100),
    'h': hp.uniform('h', 0, 90)
}

best = fmin(
    fn=objective, # Objective Function to optimize
    space=space, # Hyperparameter's Search Space
    algo=tpe.suggest, # Optimization algorithm (representative TPE)
    max_evals=100 # Number of optimization attempts
)
print(best)


'''
val = os.system('python3.8 tmp_HO.py')
with open('dict.p', 'rb') as f:
    space = p.load(f)
'''

